package experiments;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Field;

import org.junit.Test;

public class ReflectionUnitTest4 {

  @Test
  public void testMain() throws Exception {
    Simple s = new Simple();
    Field[] fields = s.getClass().getFields();
    assertEquals(1, fields.length);
    for (Field f : fields) {
      assertEquals("x", f.getName());
      assertEquals(int.class, f.getType());
      assertEquals(10, f.getInt(s));
    }
  }

  public static class Simple {
    public int x = 10;
  }
}

