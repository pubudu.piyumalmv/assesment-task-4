package experiments;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ReflectionUnitTest3 {
	
	@Test
	public void testReflection03() {
		Simple s = new Simple();
		String className = s.getClass().getName();
		assertEquals("experiments.Simple", className);
	}
	
}

