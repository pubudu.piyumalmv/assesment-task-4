package experiments;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Field;

import org.junit.Test;

public class ReflectionUnitTest5 {
  @Test
  public void testReflection05() throws Exception {
    Simple s = new Simple();
    Field[] fields = s.getClass().getDeclaredFields();
    int expectedNumFields = 2;
    assertEquals(expectedNumFields, fields.length);

    for (Field f : fields) {
      if (f.getName().equals("x")) {
        assertEquals("int", f.getType().getSimpleName());
        assertEquals(1, f.getInt(s));
      } else if (f.getName().equals("y")) {
        assertEquals("double", f.getType().getSimpleName());
        assertEquals(2.0, f.getDouble(s), 0.0);
      }
    }
  }
}
