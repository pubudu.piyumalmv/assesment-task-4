package experiments;

public class Reflection02 {
  public static void main(String[] args) {
    Simple s = new Simple();
    s.squareA();
    // s.squareB(); // if you uncomment this you will get a compiler error
    int a = s.a;
    // int b = s.b; // if you uncomment this you will get a compiler error
    System.out.println("s=" + s);
  }
}
