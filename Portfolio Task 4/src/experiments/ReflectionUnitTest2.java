package experiments;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ReflectionUnitTest2 {

    @Test
    public void testMapOutput() {
        Map<String, Integer> s = new HashMap<>();
        s.put("a", 100);
        s.put("b", 20);

        Map<String, Integer> expected = new HashMap<>();
        expected.put("a", 100);
        expected.put("b", 20);

        assertEquals(expected, s);
    }
}
