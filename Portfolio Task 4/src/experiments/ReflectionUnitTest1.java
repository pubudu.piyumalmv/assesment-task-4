package experiments;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ReflectionUnitTest1 {

    @Test
    public void testStringValues() {
        String s = "(a:10, b:20)";
        String expectedA = "10";
        String expectedB = "20";
        
        int startIndexA = s.indexOf("a:") + 2;
        int endIndexA = s.indexOf(", b");
        String actualA = s.substring(startIndexA, endIndexA);
        
        int startIndexB = s.indexOf("b:") + 2;
        int endIndexB = s.length() - 1;
        String actualB = s.substring(startIndexB, endIndexB);
        
        assertEquals(expectedA, actualA);
        assertEquals(expectedB, actualB);
    }
}
