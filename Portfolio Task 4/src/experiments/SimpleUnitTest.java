package experiments;

import org.junit.Test;
import static org.junit.Assert.*;

public class SimpleUnitTest {

@Test
public void testSquareA() {
	Simple simple = new Simple(2, 3);
	simple.squareA();
	assertEquals(4, simple.getA());
}

@Test
public void testSetA() {
	Simple simple = new Simple();
	simple.setA(5);
	assertEquals(5, simple.getA());
}

@Test
public void testSetB() {
	Simple simple = new Simple();
	simple.setB(6);
	assertEquals(6, simple.getB());
}

@Test
public void testToString() {
	Simple simple = new Simple(2, 3);
	assertEquals("(a:2, b:3)", simple.toString());
}

}

